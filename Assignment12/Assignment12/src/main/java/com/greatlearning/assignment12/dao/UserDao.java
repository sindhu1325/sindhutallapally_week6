package com.greatlearning.assignment12.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.greatlearning.assignment12.bean.User;
@Repository
public interface UserDao extends JpaRepository<User, String> {
User findByEmailAndPassword(@Param("email")String email, @Param("password")String password);
}
