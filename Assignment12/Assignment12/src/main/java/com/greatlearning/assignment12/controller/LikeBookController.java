package com.greatlearning.assignment12.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.assignment12.bean.Likes;
import com.greatlearning.assignment12.service.LikeBookService;

@RestController
@RequestMapping("/admin/likebook")
@EntityScan(basePackages = "com.greateleaning.assignment9.bean")
public class LikeBookController {
@Autowired
LikeBookService likeBookService;
@GetMapping(value="getlikeBooks",produces = MediaType.APPLICATION_JSON_VALUE)
public List<Likes> getAlllikeBookInfo()
{
	return likeBookService.getAllBooks();
}
}
