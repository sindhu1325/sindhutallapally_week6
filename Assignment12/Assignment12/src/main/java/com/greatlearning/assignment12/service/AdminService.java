package com.greatlearning.assignment12.service;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.greatlearning.assignment12.bean.Admin;
import com.greatlearning.assignment12.bean.User;
import com.greatlearning.assignment12.dao.AdminDao;

@Service
public class AdminService {
@Autowired
AdminDao adminDao;
public Admin login(String email,String password)
{
	Admin admin = adminDao.findByEmailAndPassword(email, password);
	return admin;
}
public String addAdminINfo(Admin admin )
{
	if(adminDao.existsById(admin.getEmail()))
	{
		return "this email id already present present";
	}
	else
	{
		adminDao.save(admin);
		return " User Information store";
	}
}
}
