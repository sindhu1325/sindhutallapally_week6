package com.greatlearning.assignment12.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.assignment12.bean.Book;
import com.greatlearning.assignment12.bean.Likes;
import com.greatlearning.assignment12.dao.LikeBookDao;

@Service
public class LikeBookService {
@Autowired
LikeBookDao likeBookDao;
public List<Likes> getAllBooks()
{
	return likeBookDao.findAll();
}
}
