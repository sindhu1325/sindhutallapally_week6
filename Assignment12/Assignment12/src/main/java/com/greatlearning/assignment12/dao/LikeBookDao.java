package com.greatlearning.assignment12.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.greatlearning.assignment12.bean.Likes;
@Repository
public interface LikeBookDao extends JpaRepository<Likes, Integer> {

}
