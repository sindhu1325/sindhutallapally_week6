import { Component, OnInit } from '@angular/core';
import { User , loginUser, loginAdmin} from '../user';
import { Book } from '../book';
import { NgForm } from '@angular/forms';
import { UserService } from '../user.service';
import { NgModule } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  books:Array<Book>=[];
  users:Array<User>=[];
  storemsg:string="";
   bookid:number=0;
   bookname:string="";
   image:string="";
 booktype:string=""
   authorname:string="";
   msg:string="";
deleteMsg:string="";
flag:boolean=false;
public show:boolean = false;
  public hide:boolean = true;
  public buttonName:any = 'Show';
  public nameT:any = 'Hide';
updateMsg:string="";
firstname:string="";
lastname:string="";
email:string="";
password:string ="";  
  constructor(public pser:UserService, public  router:Router) { }

  ngOnInit(): void {
  this.loadProducts()
  
  }
  loadProducts(): void{
    // console.log("event fired")
    this.pser.loadProductDetails().subscribe(res=>this.books=res);
   // this.pser.loadProductDetails();
   }
   deleteProduct(pid:number)
{
  //console.log(pid)
  this.pser.deleteProductDetails(pid).subscribe(res=>this.deleteMsg=res,error=>console.log(error),()=>this.loadProducts());
}
updateProducts(updateRef:NgForm)
{
  console.log(updateRef.value);
}
updateProduct(product:Book)
{
  console.log(product);
  this.flag=true;
  this.bookid=product.bookid;
  this.booktype=product.booktype;
  this.bookname=product.bookname;
  this.authorname=product.authorname;

}
updateProductdetails()
{
  let product={"bookid":this.bookid,"booktype":this.booktype,"bookname":this.bookname,"authorname":this.authorname}
 // console.log(product);
  this.pser.updateProductinfo(product).subscribe(result=>this.updateMsg,error=>console.log(error),
  ()=>{
    this.loadProducts();
    this.flag=false;
  }
  );
}
Hide()
{
  this.hide=!this.hide;
  if(this.hide)
  {
    
    this.nameT= "Hide";

  }
  else{
    this.nameT = "Show";
  }
}
loadUsers(): void{
  // console.log("event fired")
  this.pser.loadUserDetails().subscribe(res=>this.users=res);
 // this.pser.loadProductDetails();
 }
 deleteUser(email:string)
 {
   //console.log(pid)
   this.pser.deleteUsersDetails(email).subscribe(res=>this.deleteMsg=res,error=>console.log(error),()=>this.loadProducts());
 }
 updateUser(product:User)
{
  console.log(product);
  this.flag=true;
  this.firstname=product.firstname;
  this.lastname=product.lastname;
  this.email=product.email;
  this.password=product.password;

}
updateUserdetails()
{
  let product={"firstname":this.firstname,"lastname":this.lastname,"email":this.email,"password":this.password}
 // console.log(product);
  this.pser.updateUserinfo(product).subscribe(result=>this.updateMsg,error=>console.log(error),
  ()=>{
    this.loadProducts();
    this.flag=false;
  }
  );
}
}
