create database week6assignment_sindhu;
use week6assignment_sindhu;


create table genres(gid int Auto_Increment primary key,gname varchar(100) not null);
insert into genres(gname) 
values('Action'),('Thriller'),('Drama'),('Adventure'),('Fiction'),('Horror'),('Comedy'),('Crime'),('Mystery');


create table movies_coming(id int auto_increment primary key,name varchar(500) not null,year int,genre int,constraint fk_genre foreign key(genre) references genres(gid),ratings int,contentRating double,storyLine varchar(500) ); 

insert into movies_coming(name,year,genre,ratings,contentRating,storyLine) values('Game Night','2018',1,'10','11','A group of friends who meet regularly for game nights find themselves trying to solve a murder mystery.') ;
insert into movies_coming(name,year,genre,ratings,contentRating,storyLine) values('Area X: Annihilation','2018',4,'7','6.8','A biologists husband disappears. She puts her name forward for an expedition into an environmental disaster zone, but does not find what shes expecting. The expedition team is made up of the biologist, an anthropologist, a psychologist, a surveyor, and a linguist');
insert into movies_coming(name,year,genre,ratings,contentRating,storyLine) values('Hannah','2017',3,'9','','Intimate portrait of a woman drifting between reality and denial when she is left alone to grapple with the consequences of her husbands imprisonment');
insert into movies_coming(name,year,genre,ratings,contentRating,storyLine) values('The Lodgers','2017',6,'7','8','1920, rural Ireland. Anglo Irish twins Rachel and Edward share a strange existence in their crumbling family estate. Each night, the property becomes the domain of a sinister presence (The Lodgers) which enforces three rules upon the twins: they must be in bed by midnight; they may not permit an outsider past the threshold; if one attempts to escape, the life of the other is placed in jeopardy. When troubled war veteran Sean returns to the nearby village, he is immediately drawn to the mysterious Rachel, who in turn begins to break the rules set out by The Lodgers. The consequences pull Rachel into a deadly confrontation with her brother - and with the curse that haunts them.');
insert into movies_coming(name,year,genre,ratings,contentRating,storyLine) values('Beast of Burden','2018',1,'9','7','Sean Haggerty only has an hour to deliver his illegal cargo. An hour to reassure a drug cartel, a hitman, and the DEA that nothing is wrong. An hour to make sure his wife survives. And he must do it all from the cockpit of his Cessna.') ;


create table movies_in_theatres(id int auto_increment primary key,name varchar(500) not null,year date,genre int,constraint fk_genre1 foreign key(genre) references genres(gid),ratings int,contentRating double,storyLine varchar(500)); 
insert into movies_in_theatres(name,year,genre,ratings,contentRating,storyLine) values( 'Black Panther','2018',1,'8','15','After the events of Captain America: Civil War, King T Challa returns home to the reclusive, technologically advanced African nation of Wakanda to serve as his countrys new leader. However, TChalla soon finds that he is challenged for the throne from factions within his own country. ');
insert into movies_in_theatres(name,year,genre,ratings,contentRating,storyLine) values('Grottmannen Dug','2018',7,'7','6','Set at the dawn of time, when prehistoric creatures and woolly mammoths roamed the earth, Early Man tells the story of Dug, along with sidekick Hognob as they unite his tribe against a mighty enemy Lord Nooth and his Bronze Age City to save their home.') ;
insert into movies_in_theatres(name,year,genre,ratings,contentRating,storyLine) values('Aiyaary','2018',1,'9','7','Two officers with patriotic hearts suddenly have a fallout. The mentor, Colonel Abhay Singh has complete faith in the countrys system while Major Jai Bakshi thinks differently due to a recent stint in surveillance') ;
insert into movies_in_theatres(name,year,genre,ratings,contentRating,storyLine) values('Samson','2018',3,'6','13','A Hebrew with an unusual gift of strength must respond properly to the call of God on his life in order to lead his people out of enslavement. After his youthful ambition leads to a tragic marriage, his acts of revenge thrust him into direct conflict with the Philistine army. As his brother mounts a tribal rebellion, only Samsons relationship with a Philistine seductress and his final surrender - both to the Philistines and to God - turns imprisonment and blindness into final victory.') ;
insert into movies_in_theatres(name,year,genre,ratings,contentRating,storyLine) values('Loveless','2017',6,'7','7','Still living under the same roof, the Moscow couple of Boris and Zhenya is in the terrible final stages of a bitter divorce. Under those circumstances, as both have already found new partners, the insults pour down like rain in this toxic familial battle zone. ')  ;


create table top_rated_indian(id int auto_increment primary key,name varchar(500) not null,year int,genre int,constraint fk_genre2 foreign key(genre) references genres(gid),ratings int,contentRating double,storyLine varchar(500)); 

insert into top_rated_indian(name,year,genre,ratings,contentRating,storyLine) values( 'Anand','1971',6,'8','15','A melodramatic tale of a man with a terminal disease. The story begins with Dr Bhaksar winning a literary prize for his book about a patient called Anand. The rest is flashback. Anand, the title character, is suffering from lymphosarcoma of the intestine. He, however appears to be cheerful on the outside and is determined to extract as much pleasure from his remaining lifespan as is possible.');

insert into top_rated_indian(name,year,genre,ratings,contentRating,storyLine) values( 'Dangal','2016',1,'8','4','Biopic of Mahavir Singh Phogat, who taught wrestling to his daughters Babita Kumari and Geeta Phogat. Geeta Phogat was Indias first female wrestler to win at the 2010 Commonwealth Games, where she won the gold medal (55 kg) while her sister Babita Kumari won the silver (52 kg)');

insert into top_rated_indian(name,year,genre,ratings,contentRating,storyLine) values( 'Drishyam','2013',2,'9','6','Georgekutty (Mohanlal) is a cable TV network owner in a remote and hilly village in Kerala. He lives a happy life with his wife and 2 girls. After a few days a guy who had been at the school trip with her meets Anju. He blackmails her with a video of her that he had captured during the school trip. In the course of events he is accidentally killed by Rani (Meena) and Anju.');

insert into top_rated_indian(name,year,genre,ratings,contentRating,storyLine) values( 'Nayakan','1987',8,'7','6','A small boy (Ratnavelu) from Tamilnadu sees his father, a labor leader, killed in cold blood by a policeman. He kills the policeman and runs away to the city of Bombay. From there, the story traces his ascent to become a mafia chief (Velu Naicker)');

insert into top_rated_indian(name,year,genre,ratings,contentRating,storyLine) values( '3 Idiots','2009',7,'9','11','Farhan Qureshi and Raju Rastogi want to re-unite with their fellow collegian, Rancho, after faking a stroke aboard an Air India plane, and excusing himself from his wife - trouser less - respectively. Enroute, they encounter another student, Chatur Ramalingam, now a successful businessman, who reminds them of a bet they had undertaken 10 years ago. The trio race to locate Rancho, at his last known address - little knowing the secret that was kept from them all this time.');


create table top_rated_movies(id int auto_increment primary key,name varchar(500) not null,year int,genre int,constraint fk_genre3 foreign key(genre) references genres(gid),ratings int,contentRating double,storyLine varchar(500) ); 

insert into top_rated_movies(name,year,genre,ratings,contentRating,storyLine) values( 'Baazigar','1993',8,'6','5','Widowed Madan Chopra lives a very wealthy lifestyle with two daughters, Seema and Priya. His passion is car racing, and realizing that he is not young anymore, has his last race and wins - only to find out that another competitor, Vicky Malhotra, let him win. Madan befriends Vicky, introduces him to Priya and invites him to work for him in Bombay.');

insert into top_rated_movies(name,year,genre,ratings,contentRating,storyLine) values( '24','2016',1,'7','6','A Sci-Fi family revenge drama happening between a scientist, his evil brother and the scientists son, over a time travel gadget.');

insert into top_rated_movies(name,year,genre,ratings,contentRating,storyLine) values( 'Jodhaa Akbar','2008',3,'8','5','Jodhaa Akbar is a sixteenth century love story about a marriage of alliance that gave birth to true love between a great Mughal Emperor, Akbar and a Rajput princess, Jodhaa. Politically, success knew no bounds for Emperor Akbar,  The daughter of King Bharmal of Amer, Jodhaa resented being reduced to a mere political pawn in this marriage of alliance, and Akbars biggest challenge now did not merely lie in winning battles, but in winning the love of Jodhaa ');

insert into top_rated_movies(name,year,genre,ratings,contentRating,storyLine) values( 'Wake Up Sid','2009',2,'8','15','In Mumbai, Sid Mehra is, in the words of his father, an arrogant, spoiled brat. He lives with a doting mother, subservient brother, and a father who covers his expenses and credit card bills.He shows her the city and helps her refurbish a rented flat. He asks if she would like to progress from friend to something more, but she says no: he lacks ambition and isnt her type. Will her words, his exam results, a confrontation with his parents, and a break with his friends be enough to wake Sid up?');

insert into top_rated_movies(name,year,genre,ratings,contentRating,storyLine) values( 'Saala Khadoos','2016',1,'5','13','An under-fire boxing coach, Prabhu is transferred from Hisar in Haryana to Chennai as his bosses at the Boxing Council do not like his disrespectful rule-breaking unconventional ways. In Chennai, he chances upon the raw fighting talent of Madhi, the sibling of aspiring boxer Lakshmi. He decides to put all his effort to tame Madhie and get her to shine but he is repeatedly put off by her playfulness & distrust');



create table favourites(id int auto_increment primary key,name varchar(500) not null,year int,genre int,constraint fk_genre4 foreign key(genre) references genres(gid),ratings int,contentRating double,storyLine varchar(500)); 

insert into favourites(name,year,genre,ratings,contentRating,storyLine) values( 'Dangal','2016',1,'8','4','Biopic of Mahavir Singh Phogat, who taught wrestling to his daughters Babita Kumari and Geeta Phogat. Geeta Phogat was Indias first female wrestler to win at the 2010 Commonwealth Games, where she won the gold medal (55 kg) while her sister Babita Kumari won the silver (52 kg)');

insert into favourites(name,year,genre,ratings,contentRating,storyLine) values( 'Black Panther','2018',1,'8','15','After the events of Captain America: Civil War, King T Challa returns home to the reclusive, technologically advanced African nation of Wakanda to serve as his countrys new leader. However, TChalla soon finds that he is challenged for the throne from factions within his own country. ');

select * from genres;
select * from movies_coming;
select * from movies_in_theatres;
select * from top_rated_indian;
select * from top_rated_movies;
select * from favourites;