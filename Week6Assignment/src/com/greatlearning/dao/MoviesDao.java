package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.Favourites;
import com.greatlearning.bean.MoviesComing;
import com.greatlearning.bean.MoviesInTheatres;
import com.greatlearning.bean.TopRatedIndian;
import com.greatlearning.bean.TopRatedMovies;
import com.greatlearning.resource.MovieResource;

public class MoviesDao {
	public List<MoviesComing> retrieveMoviesComing() {
		List<MoviesComing> listOfMoviesComing = new ArrayList<>();
		try {
		//	Class.forName("com.mysql.jdbc.Driver");
			Connection con = MovieResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from movies_coming;");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					MoviesComing m = new MoviesComing();
					
					m.setId(rs.getInt(1));
					m.setTitle(rs.getString(2));
					m.setYear(rs.getInt(3));
					m.setGenre(rs.getInt(4));
					m.setRatings(rs.getInt(5));
		            m.setContentRating(rs.getInt(6));
					m.setStoryLine(rs.getString(7));
					listOfMoviesComing.add(m);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfMoviesComing;
	}
	public List<MoviesInTheatres> retrieveMoviesInTheatres() {
		List<MoviesInTheatres> listOfMoviesInTheatres = new ArrayList<>();
		try {
		//	Class.forName("com.mysql.jdbc.Driver");
			Connection con = MovieResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from movies_in_theatres;");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					MoviesInTheatres m = new MoviesInTheatres();
					
					m.setId(rs.getInt(1));
					m.setTitle(rs.getString(2));
					m.setYear(rs.getInt(3));
					m.setGenre(rs.getInt(4));
					m.setRatings(rs.getInt(5));
		            m.setContentRating(rs.getInt(6));
					m.setStoryLine(rs.getString(7));
					listOfMoviesInTheatres.add(m);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfMoviesInTheatres;
	}
	public List<TopRatedIndian> retrieveTopRatedIndian() {
		List<TopRatedIndian> listOfTopRatedIndian = new ArrayList<>();
		try {
		//	Class.forName("com.mysql.jdbc.Driver");
			Connection con = MovieResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from top_rated_indian;");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					TopRatedIndian mt = new TopRatedIndian();
					
					mt.setId(rs.getInt(1));
					mt.setTitle(rs.getString(2));
					mt.setYear(rs.getInt(3));
					mt.setGenre(rs.getInt(4));
					mt.setRatings(rs.getInt(5));
		            mt.setContentRating(rs.getInt(6));
					mt.setStoryLine(rs.getString(7));
					listOfTopRatedIndian.add(mt);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfTopRatedIndian;
	}
	public List<TopRatedMovies> retrieveTopRatedMovies() {
		List<TopRatedMovies> listOfTopRatedMovies = new ArrayList<>();
		try {
		//	Class.forName("com.mysql.jdbc.Driver");
			Connection con = MovieResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from top_rated_movies;");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					TopRatedMovies mt = new TopRatedMovies();
					
					mt.setId(rs.getInt(1));
					mt.setTitle(rs.getString(2));
					mt.setYear(rs.getInt(3));
					mt.setGenre(rs.getInt(4));
					mt.setRatings(rs.getInt(5));
		            mt.setContentRating(rs.getInt(6));
					mt.setStoryLine(rs.getString(7));
					listOfTopRatedMovies.add(mt);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfTopRatedMovies;
	}
	public List<Favourites> retrieveFavourites() {
		List<Favourites> listOfFavourites = new ArrayList<>();
		try {
		//	Class.forName("com.mysql.jdbc.Driver");
			Connection con = MovieResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from favourites;");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					Favourites mt = new Favourites();
					
					mt.setId(rs.getInt(1));
					mt.setTitle(rs.getString(2));
					mt.setYear(rs.getInt(3));
					mt.setGenre(rs.getInt(4));
					mt.setRatings(rs.getInt(5));
		            mt.setContentRating(rs.getInt(6));
					mt.setStoryLine(rs.getString(7));
					listOfFavourites.add(mt);
			}
			} catch (Exception e) {
				System.out.println("In delete method "+e);
			}
		return listOfFavourites;
	}
	
			
}
