package com.greatlearning.bean;

public class MoviesInTheatres {
	private int id;
	private String title;
	private int year;
	private int genre;
	private int ratings;
	private int contentRating;
	private String storyLine;
	
	
	public MoviesInTheatres() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getGenre() {
		return genre;
	}

	public void setGenre(int genre) {
		this.genre = genre;
	}

	public int getRatings() {
		return ratings;
	}

	public void setRatings(int ratings) {
		this.ratings = ratings;
	}

	public int getContentRating() {
		return contentRating;
	}

	public void setContentRating(int contentRating) {
		this.contentRating = contentRating;
	}

	public String getStoryLine() {
		return storyLine;
	}

	public void setStoryLine(String storyLine) {
		this.storyLine = storyLine;
	}

	public MoviesInTheatres(int id, String title, int year, int genre, int ratings, int contentRating,
			String storyLine) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.genre = genre;
		this.ratings = ratings;
		this.contentRating = contentRating;
		this.storyLine = storyLine;
	}

	@Override
	public String toString() {
		return "MoviesInTheatres [id=" + id + ", title=" + title + ", year=" + year + ", genre=" + genre + ", ratings="
				+ ratings + ", contentRating=" + contentRating + ", storyLine=" + storyLine +  "]";
	}
	
	
	

}
