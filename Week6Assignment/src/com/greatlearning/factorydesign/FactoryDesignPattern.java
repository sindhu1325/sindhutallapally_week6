package com.greatlearning.factorydesign;

interface Movies{
	public void create();
}

class MoviesComing implements Movies{
	public void create() {
		System.out.println("Movies Coming");
	}
}
class MoviesInTheatres implements Movies{
	public void create() {
		System.out.println("Movies in Theatres");
	}
}
class TopRatedMovies implements Movies{
	public void create() {
		System.out.println("Top Rated Movies");
	}
}
class TopRatedIndian implements Movies{
	public void create() {
		System.out.println("Top Rated Indian");
	}
}
class Favourites implements Movies{
	public void create() {
		System.out.println("Favourites");
	}
}
class MoviesFactory{
	public static Movies getInstance(String type) {
		if(type.equalsIgnoreCase("moviescoming")) {
			return new MoviesComing();
		}else if (type.equalsIgnoreCase("moviesintheatres")) {
			return new MoviesInTheatres();
		}else if(type.equalsIgnoreCase("topratedindian")) {
			return new TopRatedIndian();
		}else if(type.equalsIgnoreCase("topratedmovies")) {
			return new TopRatedMovies();
		}else if(type.equalsIgnoreCase("favourites")) {
			return new Favourites();
		}else {
			return null;
		}
	}
}

public class FactoryDesignPattern {
	public static void main(String[] args) {
	Movies m = 	MoviesFactory.getInstance("moviescoming");
		m.create();

	}

}
