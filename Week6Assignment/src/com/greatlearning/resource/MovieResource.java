package com.greatlearning.resource;

import java.sql.Connection;
import java.sql.DriverManager;

public class MovieResource {

	private static Connection con;
	private MovieResource() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week6assign","root","Chandubro@25");
		} catch (Exception e) {
			System.out.println("Db Connection error "+e);
		}
	}
	public static Connection getDbConnection() {
		try {
			return con;
		} catch (Exception e) {}
		return null;
	}
	
	// This method from main call at the last once all operation work finish. 
	public static void closeConnection() {
		try {
			con.close();
		} catch (Exception e) {}
	}
	
}
